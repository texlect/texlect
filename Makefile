SUBDIRS = texlect \
		  examples/labreport \
		  examples/thmtest \
		  examples/research_report \
		  examples/research_presentation \
		  man/gost \
		  man/pythontex \
		  man/pscyr \
		  task \

all: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -j4 -C $@
	cp $@/*.pdf Output

.PHONY: $(SUBDIRS)
#.PHONY: clean

clean:
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir $@; \
	done
	rm -fv Output/*.pdf
