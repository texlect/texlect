#!/bin/sh

# Данный сценарий управляет компиляцией презентаций всех лекций.
# Код презентации и раздаточного материала так же продублирован в файлах
# lecture.tex и handout.tex

# Названия лекций
declare -a LNAMES=(
        
        "Вводная лекция"
        "Классы и рубрикация документов научных работ"
        "Типографский набор. Шрифты и форматирование текста"
        "Математические формулы, символы, теоремы. \AmS-\LaTeX"
        "Рисунки и таблицы. Использование PGFPlots и TikZ"
        "Разработка программной документации: алгоритмы, псевдокод и листинги"
        "Разработка больших проектов \LaTeX ~для научных работ в соответствии с
        требованиями ГОСТ. Библиография и цитирование"
        "Программирование в \LaTeX. Создание пользовательских пакетов. Межъязыковое
        взаимодействие"
        
        )

# Число лекций
LNUMBER=${#LNAMES[@]}

# Число запуска pdflatex
TEXNUMBER=3

for lname in "${LNAMES[@]}"; do
for lnumber in `seq $LNUMBER`; do
for starttex in `seq $TEXNUMBER`; do

# Компиляция лекций
pdflatex -jobname=lecture$lnumber <<EOF

\documentclass[ucs,serif,color=usenames,xcolor=table,xcolor=dvipsnames]{beamer}

\input{beamerconf}          % Настройки beamer
\input{packages}            % Подключаемые пакеты
\input{lstconf}             % Стили кода
\input{styles}              % Пользовательские стили

\begin{document}

\input{../dvars}

\title[Разработка научно-технической документации в издательской
    системе ]{\dtitle}
\subtitle{Лекция $lnumber: ${LNAMES[$lnumber-1]}}
%\author{\dauthor, \space \dadegree}
\author{\damail}
\institute{\includegraphics[scale=0.15]{./pfiles/pics/logo.png}\\ \dcaf}
\date{\mayday}

\input{figureconf}              % Настройка рисунков
\input{title}
\input{contents}                % Оглавление
\input{./lect/lecture$lnumber}  % Материал лекции
\input{appendix}                % Общая информация

\end{document} 

EOF

# Компиляция раздаточного материала
#pdflatex -jobname=handout$lnumber <<EOF
#
#\documentclass[handout,ucs,serif,color=usenames,xcolor=table,xcolor=dvipsnames]{beamer}
#
#\input{beamerconf}
#\input{packages} 
#\input{lstconf} 
#\input{styles} 
#
#\usepackage[absolute,overlay]{textpos}
#\usepackage{pgfpages}
#\usepackage{./pfiles/sty/handoutWithNotes}
#
#\pgfpagesuselayout{1 on 1 with notes}[a4paper,border shrink=5mm]
#%\pgfpagesuselayout{1 on 1 with notes landscape}[a4paper,border shrink=5mm]
#%\pgfpagesuselayout{2 on 1 with notes}[a4paper,border shrink=5mm]
#%\pgfpagesuselayout{2 on 1 with notes landscape}[a4paper,border shrink=5mm]
#%\pgfpagesuselayout{3 on 1 with notes}[a4paper,border shrink=5mm]
#%\pgfpagesuselayout{4 on 1 with notes}[a4paper,border shrink=5mm]
#
#\begin{document}
#
#\input{../dvars}
#
#\title[Разработка научно-технической документации в издательской системе ]{\dtitle}
#
#\subtitle{Лекция $lnumber: ${LNAMES[$lnumber-1]}}
#%\author{\dauthor, \space \dadegree}
#\author{\damail}
#%\institute{\includegraphics[scale=0.10]{./pfiles/pics/logo.png}\\ \dcaf}
#\date{\mayday}
# 
#\input{figureconf}          % Настройка рисунков
#\input{title}
#\input{contents}            % Оглавление
#\input{./lect/lecture$lnumber}    % Материал лекции
#\input{appendix}            % Общая информация
# 
#\end{document} 
#
#EOF
#
done
done
done
