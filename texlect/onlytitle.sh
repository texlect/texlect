#!/bin/sh

# Данный сценарий управляет компиляцией титульного листа раздаточного материала.
# Код титульного листа раздаточного материала так же продублирован в файле
# onlytitle.tex

for i in `seq 1`; do
pdflatex -jobname=onlytitle$i <<EOF

% Настройки титульной страницы раздаточного материала без заметок

% Установить 'ucs' для русскоязычного меню файла PDF 
\documentclass[portrait,ucs,serif,color=usenames,xcolor=dvipsnames]{beamer}
\usepackage[orientation=portrait,size=A4]{beamerposter}

\input{beamerconf}          % Настройки beamer
\input{packages}            % Подключаемые пакеты
\input{lstconf}             % Стили кода
\input{styles}              % Пользовательские стили
\input{titleconf_foronly}   % Настройки титульной страницы презентации

\begin{document}

\input{title}               % Титульная страница

\end{document} 

EOF
done
