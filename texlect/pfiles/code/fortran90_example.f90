! if x is ouside the xi(1)-xi(ni) interval take a boundary value
if (xx <= xi(1)) then
  lagrint = yi(1)
  return
end if
if (xx >= xi(ni)) then
  lagrint = yi(ni)
  return
end if
